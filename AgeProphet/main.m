
//
//  main.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/01/30.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
