//
//  HomeViewController.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/07/15.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import "HomeViewController.h"
#import "ImageViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// ナビゲーションバー
    self.navigationController.navigationBar.tintColor = [UIColor brownColor];
    
    // 背景画像
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"HomeViewController#prepareForSegue start");
    if ( [[segue identifier] isEqualToString:@"PushFromZodiacToImage"] ) {
        ImageViewController *nextViewController = [segue destinationViewController];
        
        nextViewController.zodiacIndex = -1;
    }
    NSLog(@"HomeViewController#prepareForSegue end");
}

@end
