//
//  ControllerHelper.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/09/07.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import "ControllerHelper.h"

@implementation ControllerHelper
- (void)initController:(UIViewController *)controller
{
    // 背景画像
    UIGraphicsBeginImageContext(controller.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:controller.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    controller.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
}
@end
