//
//  SaveViewController.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/09/15.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import "SaveViewController.h"
#import "ImageViewController.h"
#import "HomeViewController.h"
#import "FMDatabase.h"

/**
 TODO
 削除機能。
 */
@interface SaveViewController ()
@end

@implementation SaveViewController
@synthesize theManZodiacIndex;
@synthesize theManZodiacName;
@synthesize theManBirthYear;
@synthesize theManId;

UIAlertView *firstAlert;
UIAlertView *secondAlert;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/**
 * 遷移前のイベント
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"PushToImage"] ) {
        ImageViewController *nextViewController = [segue destinationViewController];
        NSLog(@"SaveViewController theManZodiacIndexは%d", self.theManZodiacIndex);
        
        nextViewController.zodiacIndex = self.theManZodiacIndex;
    }
}

/** 干支画像表示 */
- (void)dispZodiacImage
{
    NSString *imageURL;
    // 干支画像
    // TODO 干支のインデックスの定数化
    switch(self.theManZodiacIndex){
        case 0:
            imageURL = @"nezumi.png";
            break;
            
        case 1:
            imageURL = @"usi.png";
            break;
            
        case 2:
            imageURL = @"tora.png";
            break;
            
        case 3:
            imageURL = @"usagi.png";
            break;
            
        case 4:
            imageURL = @"ryu.png";
            break;
            
        case 5:
            imageURL = @"hebi.png";
            break;
            
        case 6:
            imageURL = @"uma.png";
            break;
            
        case 7:
            imageURL = @"hitsuji.png";
            break;
            
        case 8:
            imageURL = @"saru.png";
            break;
            
        case 9:
            imageURL = @"tori.png";
            break;
            
        case 10:
            imageURL = @"inu.png";
            break;
            
        case 11:
            imageURL = @"inosisi.png";
            break;
            
        default:
            imageURL = @"zodiac_all.gif";
            break;
    }
    
    self.zodiacImageView.image = [UIImage  imageNamed:imageURL];
}


- (NSString *)getZodiacName:(int)zodiacIndex
{
    NSString *_zodiacName;
    switch(zodiacIndex){
        case 0:
            _zodiacName = @"ねずみ";
            break;
            
        case 1:
            _zodiacName = @"うし";
            break;
            
        case 2:
            _zodiacName = @"とら";
            break;
            
        case 3:
            _zodiacName = @"うさぎ";
            break;
            
        case 4:
            _zodiacName = @"たつ";
            break;
            
        case 5:
            _zodiacName = @"へび";
            break;
            
        case 6:
            _zodiacName = @"うま";
            break;
            
        case 7:
            _zodiacName = @"ひつじ";
            break;
            
        case 8:
            _zodiacName = @"さる";
            break;
            
        case 9:
            _zodiacName = @"とり";
            break;
            
        case 10:
            _zodiacName = @"いぬ";
            break;
            
        case 11:
            _zodiacName = @"いのしし";
            break;
            
        default:
            _zodiacName = @"";
            break;
    }
    return _zodiacName;
}

// TODO 例外処理
- (void)doGetExsitData
{
    NSLog(@"doGetExsitData start");

    // データベース
    FMDatabase *db = [self openDatabase];

    NSNumber *_idNumber = [[NSNumber alloc] initWithInt:theManId];

    NSString* selectSql = @"SELECT id, nickname, birth_year, memo, regist_date, zodiac_index FROM member WHERE id=?;";
    NSLog(@"SQL: %@ ", selectSql);

    FMResultSet *results = [db executeQuery:selectSql, _idNumber];
    while ([results next]) {
        // 誕生年(年齢)
        theManBirthYear = [results intForColumn:@"birth_year"];
        theManZodiacIndex = [results intForColumn:@"zodiac_index"];
        theManZodiacName = [self getZodiacName:theManZodiacIndex];
        
        self.theManNickNameText.text = [results  stringForColumn:@"nickname"];
        self.memoText.text = [results  stringForColumn:@"memo"];
        self.dateText.text = [results  stringForColumn:@"regist_date"];
        
        NSLog(@"%d %@ %d %@ %@ %d", [results intForColumn:@"id"], [results stringForColumn:@"nickname"], theManBirthYear, [results stringForColumn:@"memo"], [results stringForColumn:@"regist_date"], [results intForColumn:@"zodiac_index"]);
    }
    
    //check
    if ([db hadError]) {
        NSLog(@"DB Select Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        return;
    }
    
    [db close];
    
    NSLog(@"doGetExsitData end");
}

// TODO 例外処理
- (int)getDataCount
{
    NSLog(@"getDataCount start");
    
    // データベース
    FMDatabase *db = [self openDatabase];
    
    NSString* selectSql = @"SELECT count(*) cnt FROM member;";
    NSLog(@"SQL: %@ ", selectSql);
    
    FMResultSet *results = [db executeQuery:selectSql];
    int cnt = 0;
    while ([results next]) {
        cnt = [results intForColumn:@"cnt"];
        
        NSLog(@"getDataCount cnt : %d", cnt);
    }
    
    //check
    if ([db hadError]) {
        NSLog(@"DB Select Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        return -1;
    }
    
    [db close];
    
    NSLog(@"getDataCount end");
    return cnt;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // 背景画像
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    // 今日の年月日を取得
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    NSUInteger flags;
    
    flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    comps = [calendar components:flags fromDate:now];
    
    NSLog(@"%d年 %d月 %d日", comps.year, comps.month, comps.day);

    // デフォルト登録日
    // doGetExsitDataより前に実施すること。
    self.dateText.text = [NSString stringWithFormat:@"%d/%d/%d", comps.year, comps.month, comps.day];
    
    if (theManId >= 0) {
        // 既にデータがある場合は、DBから取得。
        [self doGetExsitData];
    }
    
    // 干支画像
    [self dispZodiacImage];

    // 吹き出しのメッセージ
    self.messageText.text = [NSString stringWithFormat:@"%d 年生まれで、今年 %d 歳。%@年。\nこの画面で保存できます。", self.theManBirthYear, comps.year - theManBirthYear, theManZodiacName ];
    
    // 初回登録時は、初期フォーカスをニックネームに。
    // 既に登録したデータの表示の場合は、入力しない場合があるため、フォーカスは特に指定しない。
    if (self.theManId < 0) {
        [self.theManNickNameText becomeFirstResponder];
        self.dustButton.enabled = NO;
    }
    
    //TODO 保存ボタンの制御はいずれ
//    self.saveButton.enabled = false;
    
    // キーボードで隠れないように
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(keyboardDidShow:)
//     name:UIKeyboardDidShowNotification
//     object:nil];
//    
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(keyboardDidHide:)
//     name:UIKeyboardDidHideNotification
//     object:nil];
    
    // ニックネーム入力時のキーボードの改行ボタンを完了ボタンにする
    self.theManNickNameText.returnKeyType = UIReturnKeyDone;
    
    self.theManNickNameText.delegate = self;
    
    // メモ入力時のキーボードの改行ボタンを完了ボタンにする
    self.memoText.returnKeyType = UIReturnKeyDone;
    
    self.memoText.delegate = self;
}

/** キーボードで潜れない処理 */
- (void)keyboardDidShow:(NSNotification *)aNotification
{
    CGRect keyboardRect = [self.view
                           convertRect:[[[aNotification userInfo]
                                         objectForKey:UIKeyboardFrameEndUserInfoKey]
                                        CGRectValue]
                           toView:nil];
    CGRect viewRect = self.view.frame;
    viewRect.size.height -= (keyboardRect.size.height
                             - self.navigationController.toolbar.frame.size.height);
    self.view.frame = viewRect;
}


- (IBAction)nickNameDitValueChanged:(id)sender {
    
}

- (FMDatabase *)openDatabase {
    NSArray*    paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    
    NSString*   dir   = [paths objectAtIndex:0];
    NSLog(@"dir: %@ ", dir);
    
    FMDatabase* _db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"AgeProphet.db"]];
    
    [_db open];
    return _db;
}

- (IBAction)saveDidTouchUpInside:(id)sender {
    NSLog(@"SaveViewController saveDidTouchUpInside");

    FMDatabase *db = [self openDatabase];
    
    // debug
//    NSString*   deleteSql = @"DROP TABLE member";
//    [db executeUpdate:deleteSql];
    

    // 干支は誕生年が分かれば導出可能。
    // ビジネスモデル化するかどうか。
    // テーブル構成
    NSString*   createTableSql = @"CREATE TABLE IF NOT EXISTS member (id INTEGER PRIMARY KEY AUTOINCREMENT, nickname TEXT, birth_year INTEGER, memo TEXT, regist_date TEXT, zodiac_index INTEGER);";
    
    NSLog(@"SQL: %@ ", createTableSql);

    [db executeUpdate:createTableSql];
    
    //check
    if ([db hadError]) {
        NSLog(@"DB Create Table Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        [db close];
        return;
    }

    NSLog(@"nickname: %@ ", self.theManNickNameText.text);
    NSLog(@"zodiac_index: %d ", self.theManZodiacIndex);
    NSLog(@"birth_year: %d ", self.theManBirthYear);
    NSLog(@"memo: %@ ", self.memoText.text);
    NSLog(@"regist_date: %@ ", self.dateText.text);
    
    // 変換(プリミティブ型は実行時エラーになる)
    NSNumber *_zodiacIndex = [NSNumber numberWithInt:self.theManZodiacIndex];
    NSNumber *_birthYear = [NSNumber numberWithInt:self.theManBirthYear];
    NSNumber *_id = [NSNumber numberWithInt:theManId];
    
    if (theManId < 0) {
        // 新規登録
        NSString*   insertSql = @"INSERT INTO member (nickname, birth_year, memo, regist_date, zodiac_index) VALUES (?, ?, ?, ?, ?)";
        NSLog(@"SQL: %@ ", insertSql);
        
        [db executeUpdate:insertSql, self.theManNickNameText.text, _birthYear, self.memoText.text, self.dateText.text, _zodiacIndex];
        
        //check
        if ([db hadError]) {
            NSLog(@"DB Insert Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            [db close];
            return;
        }
        
        theManId = [db lastInsertRowId];
        NSLog(@"DB Insert theManId %d", theManId);
    } else {
        // すでに存在する場合は、更新。
        NSString*  updateSql = @"UPDATE member SET nickname=?, birth_year=?, memo=?, regist_date=?, zodiac_index=? WHERE id=?";
        NSLog(@"SQL: %@ ", updateSql);
        
        [db executeUpdate:updateSql, self.theManNickNameText.text, _birthYear, self.memoText.text, self.dateText.text, _zodiacIndex, _id];
        
        //check
        if ([db hadError]) {
            NSLog(@"DB Update Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            [db close];
            return;
        }
    }
    
    [db close];
    
    // ダイアログ表示
    firstAlert = [[UIAlertView alloc]
                          initWithTitle:@"Information"
                          message:@"保存しました。\nホーム画面に戻りますか？"
                          delegate:self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:@"OK", nil];
    firstAlert.alertViewStyle = UIAlertViewStyleDefault;
    [firstAlert show];
}




- (void)deleteData
{
    FMDatabase *db;
    @try {
        db = [self openDatabase];
        NSString*  deleteSql = @"DELETE FROM member WHERE id=?";
        NSLog(@"SQL: %@ ", deleteSql);
        NSLog(@"ID: %d ", theManId);
        
        // パラメータでは、要型変換
        NSNumber *_id = [NSNumber numberWithInt:theManId];
        [db executeUpdate:deleteSql, _id];
        
        //check
        if ([db hadError]) {
            NSLog(@"DB Delete Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            [db close];
            return;
        }
    }
    @catch (NSException *exception) {
        UIAlertView *alert = [[UIAlertView alloc]
         initWithTitle:@"Information"
         message:@"削除でエラーが発生しました。"
         delegate:self
         cancelButtonTitle:nil
         otherButtonTitles:@"OK", nil];
        alert.alertViewStyle = UIAlertViewStyleDefault;
        [alert show];
    }
    @finally {
        [db close];
    }
}

/**
 * アラートダイアログのボタン押下時の処理
 *
 * firstAlert:登録後の画面遷移の有無
 * secondAlert:削除するかどうか
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == firstAlert) {
        switch (buttonIndex) {
            case 0://押したボタンがCancelなら何もしない
                break;
                
            case 1://押したボタンがOKなら画面遷移
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
                break;
        }
    } else if (alertView == secondAlert) {
        switch (buttonIndex) {
            case 0://押したボタンがCancelなら何もしない
                break;
                
            case 1://押したボタンがOKなら削除して前の画面に戻る
                [self deleteData];

                // 件数を確認
                // 0件であれば、トップページ遷移(リストに戻る意味はなく、なぜかデータが表示上データリストに残って表示されてしまうため)
                // 1件以上あれば、前の画面へ遷移
                int cnt = 0;
                cnt = [self getDataCount];
                if (cnt == 0) {
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
                } else {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                break;
        }
    }
}


/**
 * 削除ボタン TouchUpInside イベントハンドラ
 */
- (IBAction)didDustButtonTouchUpInside:(id)sender {
    if (theManId < 0) {
        return;
    }
    
    // ダイアログ表示
    secondAlert = [[UIAlertView alloc]
                          initWithTitle:@"Information"
                          message:@"削除しますか？"
                          delegate:self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:@"OK", nil];
    secondAlert.alertViewStyle = UIAlertViewStyleDefault;
    [secondAlert show];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/**
 * キーボードでReturnキーが押されたとき
 * @param textField イベントが発生したテキストフィールド
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // キーボードを隠す
    [self.view endEditing:YES];
    
    return YES;
}


- (void)viewDidUnload {
    NSLog(@"SaveViewController#viewDidUnload start");
    self.theManNickNameText.delegate = nil;
    self.memoText.delegate = nil;
    
    [super viewDidUnload];
    NSLog(@"SaveViewController#viewDidUnload end");
}

- (void)dealloc {
    NSLog(@"SaveViewController#dealloc start");
    self.theManNickNameText.delegate = nil;
    self.memoText.delegate = nil;
    NSLog(@"SaveViewController#dealloc end");
}

@end
