//
//  ImageViewController.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/02/15.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController
@synthesize zodiacIndex = _value;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // 背景画像
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];

    
    NSString *imageURL;

    NSLog(@"ImageViewController zodiacIndexは%d", self.zodiacIndex);
    

    // TODO 干支のインデックスの定数化
    switch(self.zodiacIndex){
    case 0:
        imageURL = @"nezumi.png";
        break;
        
    case 1:
        imageURL = @"usi.png";
        break;

    case 2:
        imageURL = @"tora.png";
        break;
            
    case 3:
        imageURL = @"usagi.png";
        break;
            
    case 4:
        imageURL = @"ryu.png";
        break;
        
    case 5:
        imageURL = @"hebi.png";
        break;
            
    case 6:
        imageURL = @"uma.png";
        break;
            
    case 7:
        imageURL = @"hitsuji.png";
        break;
            
    case 8:
        imageURL = @"saru.png";
        break;
            
    case 9:
        imageURL = @"tori.png";
        break;
            
    case 10:
        imageURL = @"inu.png";
        break;
            
    case 11:
        imageURL = @"inosisi.png";
        break;
            
    default:
        imageURL = @"zodiac_all.gif";
        break;
    }
    
    self.ZodiacImageView.image = [UIImage  imageNamed:imageURL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
