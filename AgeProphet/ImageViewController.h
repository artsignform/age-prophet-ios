//
//  ImageViewController.h
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/02/15.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ZodiacImageView;
@property(assign, nonatomic) int zodiacIndex;

@end
