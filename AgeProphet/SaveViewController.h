//
//  SaveViewController.h
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/09/15.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaveViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property(assign, nonatomic) int theManZodiacIndex; // その人の干支を表すインデックス
@property(assign, nonatomic) NSString *theManZodiacName;
@property(assign, nonatomic) int theManBirthYear; // 誕生年
@property(assign, nonatomic) int theManId; // データベースID(登録されてない場合:-1)

@property (weak, nonatomic) IBOutlet UILabel *theManZodiacLabel;

@property (weak, nonatomic) IBOutlet UILabel *theManBirthYearLabel;

@property (weak, nonatomic) IBOutlet UITextView *messageText;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (weak, nonatomic) IBOutlet UIButton *dustButton;

@property (weak, nonatomic) IBOutlet UITextField *theManNickNameText;

@property (weak, nonatomic) IBOutlet UILabel *dateText;
@property (weak, nonatomic) IBOutlet UITextField *memoText;

@property (weak, nonatomic) IBOutlet UIImageView *zodiacImageView;

@end
