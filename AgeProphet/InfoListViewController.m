//
//  InfoListViewController.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2014/02/16.
//  Copyright (c) 2014年 Toshitaka MORI. All rights reserved.
//

#import "InfoListViewController.h"
#import "AboutViewController.h"

@interface InfoListViewController ()

@end

@implementation InfoListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // 背景画像
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
}


/**
 * 遷移前のイベント
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AboutViewController *nextViewController = [segue destinationViewController];
    if ( [[segue identifier] isEqualToString:@"AboutInfo"] ) {
        nextViewController.urlString = @"http://d.hatena.ne.jp/artsignform/20140215/1392495974";
    } else if ( [[segue identifier] isEqualToString:@"UsageInfo"] ) {
        nextViewController.urlString = @"http://d.hatena.ne.jp/artsignform/20140216/1392499230";
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
