//
//  ZodiacViewController.h
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/02/17.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZodiacViewController : UIViewController
<UIPickerViewDelegate>

- (NSString *)getZodiacName: (int) index;

// TODO ピッカーが不要に 2014/02/16
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIImageView *zodiacImageView;

@end
