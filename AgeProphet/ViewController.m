//
//  ViewController.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/01/30.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//
//===========================================================
//年齢を推測するアプリ。
//
// （TODO javadocのような出力形式あるのかな？）
//
//■概要
//画面上で選択した干支と年代により年齢を推測する。
//例：今年2013年だった場合
//子年(ねずみ)の2000年代であれば、2008年生まれなので、2013-2008で5歳(誕生日を迎えてなければ4歳)。
//兎年(うさぎ)の1970年代であれば、1975年生まれなので、2013-1975で38歳(誕生日を迎えてなければ37歳)。
//
//■目的
//初対面の時など、年齢は気になるものの直接聞きづらい場面が想定される。
//特に合同コンパ(略して合コン)などのシーンにおいて、異性の年齢を聞くというのは、
//何かしらのリスクがある。
//
//そういった場合に、干支や見た目の年代などの情報を元に相手の年齢を推測することが可能。
//干支から年齢を計算することはすぐに計算が難しいため、このアプリにより年齢計算を行う。
//干支の聞き方は、少し会話のテクニークが必要かもしれないが、各自工夫してほしい。
//例えば、このアプリのアイデアを思いついたのは、2013年1月の巳年(へび)であるが、
//「蛇の年ってなんか変じゃない？ところで君、何年生まれ？」といった流れや、
//猫の話題などをさり気なくふってみて
//「ところで、ねずみ年はあるのに、ねこ年ってないよね？あれってねずみがねこを騙したらしいよ・・・うんちく話・・・
//ところで君、何年生まれ？」といった具合に工夫次第で聞くことは可能であろう。
//
//■ロジック概要
//以下の計算を行う。
//子年=1924年を基準年とする
//今年を取得する
//<今年の干支のインデックス> ・・・ (<今年> ー <基準年>)÷12の余り
//<直近で年男・年女にあたる年> ・・・ <今年> ー (<今年の干支のインデックス> ー <その人の干支のインデックス>
//生年月日の年＝<スライダーで選択した年>＋((<直近で年男・年女にあたる年> ー <選択した年>)÷12の余り)
//誕生日を迎えた人の年齢 = 「<今年> ー <生年月日>」
//誕生日を迎えてない人の年齢 = <誕生日を迎えた人の年齢> ー 1
//
//＃＃＃シミュレーション＃＃＃
//(2013年に実施 1975年生まれの38歳の兎年 1971年へスライド)
//2013-1924=89(1924基準年)
//今年の干支のインデックス・・・89/12=7 余り 5(巳年に実行)
//5-3=2(卯3 2年前に年男・年女)
//直近で年男・年女にあたる年・・・2013-2=2011
//1971年選択
//2011-1971=40
//40/12=3 余り 4
//生年月日の年・・・1971+4=1975
//誕生日を迎えた人の年齢・・・2013-1975=38歳
//誕生日を迎えてない人の年齢・・・38-1=37歳
//
//(2013年に実施 1963年生まれの50歳の兎年 1960年へスライド)
//2013-1924=89(1924基準年)
//今年の干支のインデックス・・・89/12=7 余り 5(巳年に実行)
//5-3=2(卯3 2年前に年男・年女)
//直近で年男・年女にあたる年・・・2013-2=2011
//1960年選択
//2011-1960=51
//51/12=4余り3
//生年月日の年・・・1960+3=1963
//誕生日を迎えた人の年齢・・・2013-1963=50歳
//誕生日を迎えてない人の年齢・・・50-1=49歳
//
//(2013-2008=5)
//2008-1996=12
//12/12=1 余り 0
//
//(2014年に実施 2001年生まれの13歳の巳年 2000年へスライド)
//2014-1924=90(1924基準年)
//今年の干支のインデックス・・・90/12=7 余り 6(午年に実行)
//6-5=1(巳5 1年前に年男・年女)
//直近で年男・年女にあたる年・・・2014-1=2013
//2000年選択
//2013-2000=13
//13/12=1余り1
//生年月日の年・・・2000+1=2001
//2014-2001=13歳
//13-1=12歳
//
//■干支ボタン処理
//各干支ボタンが押された時にインデックスとなる番号を保持する。
//子0、丑1、寅2、卯3、辰4、巳5、午6、未7、申8、酉9、戌10、亥11
//デフォルトは子年とする。
//
//
// TODO 実装的にはボタンではなく、ラジオボタンなどでインデックスにより処理できると楽そう。
// TODO ボタン名もわかりやすい名前(ねずみ、うし・・・)に設定変更できるようにしたい。
// TODO ボタンをクリックすると生年月日年と年齢の候補をスライダーにうまく反映したい。
//
//■スライダー処理
//・右端を今年とする(システム日付取得)
//・左端(起点)は、今年から100年マイナスした年とする
//・デフォルト値は
//
//TODO 設定画面でスライダーの範囲指定を可能にしたい。
//TODO 設定画面でデフォルト値の指定を可能にしたい。
//
//■カモフラージュ(パニック)
//とっさの場合に、ボタンを押すと、干支の画像に切り替わる。
//
//===========================================================


#import "ViewController.h"
#import "ImageViewController.h"
#import "SaveViewController.h"

@interface ViewController ()
{
    int nowYear; // 今年
    int nowZodiacIndex; // 干支インデック
    
    int baseYear; // 基準年(子年)
    
    BOOL isInputZodiac;
    BOOL isInputYear;
    
    int theManBirthYear;
}
@end

@implementation ViewController
@synthesize theManZodiacIndex;
@synthesize theManZodiacName;

/** 
 遷移前のイベント
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"PushFromYearToImage"] ) {
        ImageViewController *nextViewController = [segue destinationViewController];
        NSLog(@"ViewController theManZodiacIndexは%d", self.theManZodiacIndex);
        
        nextViewController.zodiacIndex = self.theManZodiacIndex;
    } else if ( [[segue identifier] isEqualToString:@"PushSave"] ) {
        SaveViewController *nextViewController = [segue destinationViewController];
        NSLog(@"ViewController#prepareForSegue to ViewController");
        NSLog(@"ViewController#prepareForSegue theManZodiacIndexは%d", theManZodiacIndex);
        NSLog(@"ViewController#prepareForSegue theManZodiacNameは%@", theManZodiacName);
        
        nextViewController.theManZodiacIndex = theManZodiacIndex;
        nextViewController.theManZodiacName = theManZodiacName;
        nextViewController.theManBirthYear = theManBirthYear;
        nextViewController.theManId = -1;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

//    ControllerHelper *cHelper = [[ControllerHelper alloc ] init];
//    [cHelper initController:self];

    // 背景画像
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    theManBirthYear = 0;
    self.nextButton.enabled = false;
    
    NSString *imageURL;
    // 干支画像
    // TODO 干支のインデックスの定数化
    switch(self.theManZodiacIndex){
        case 0:
            imageURL = @"nezumi.png";
            break;
            
        case 1:
            imageURL = @"usi.png";
            break;
            
        case 2:
            imageURL = @"tora.png";
            break;
            
        case 3:
            imageURL = @"usagi.png";
            break;
            
        case 4:
            imageURL = @"ryu.png";
            break;
            
        case 5:
            imageURL = @"hebi.png";
            break;
            
        case 6:
            imageURL = @"uma.png";
            break;
            
        case 7:
            imageURL = @"hitsuji.png";
            break;
            
        case 8:
            imageURL = @"saru.png";
            break;
            
        case 9:
            imageURL = @"tori.png";
            break;
            
        case 10:
            imageURL = @"inu.png";
            break;
            
        case 11:
            imageURL = @"inosisi.png";
            break;
            
        default:
            imageURL = @"zodiac_all.gif";
            break;
    }
    
    self.zodiacImageView.image = [UIImage  imageNamed:imageURL];


    // TODO 定数化
    baseYear = 1900;
   
    // 今年を取得する
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    comps = [calendar components:(NSYearCalendarUnit) fromDate:date];
    nowYear = [comps year];
    self.lastScaleEstimatingYearLabel.text = [NSString stringWithFormat:@"%d", nowYear];
    self.yearEstimationSlider.maximumValue = nowYear;
    self.ageEstimationSlider.maximumValue = 12*10;
    
    // TODO x年前は定数化。設定化。
    int startYear = nowYear - 12*10;
    self.startScaleEstimatingYearLabel.text = [NSString stringWithFormat:@"%d", startYear];
    self.yearEstimationSlider.minimumValue = startYear;
    self.ageEstimationSlider.minimumValue = 0;
    
    int selectedAge = (nowYear - startYear) / 2;
    int selectedYear = startYear + selectedAge;
    self.midScaleEstimatingYearLabel.text = [NSString stringWithFormat:@"%d", selectedYear];
    self.yearEstimationSlider.value = selectedYear;
    self.ageEstimationSlider.value = selectedAge;
    
    self.selectedEstimationYearLabel.text = [NSString stringWithFormat:@"%d", selectedYear];
    
    //<今年の干支のインデックス> ・・・ (<今年> ー <基準年>)÷12の余り
    nowZodiacIndex = (nowYear - baseYear) % 12;
    
    // 吹き出し
    self.theManZodiacText.text = [NSString stringWithFormat:@"%@年ですね。\n次は下のスライダーでその人の年齢を推定してみましょう。", theManZodiacName];
    
    isInputZodiac = YES;
    
    if (isInputYear == NO) {
        self.beforeBirthAgeLabel.text = @"？";
        self.afterBirthAgeLabel.text = @"？";
        self.selectedEstimationYearLabel.text = @"？";
    }
    
    NSLog(@"ViewController#viewDidLoad.");

}

- (void)initXXX
{
    isInputZodiac = NO;
    isInputYear = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// 干支ボタン
- (IBAction)touchUpInsideZodiacMouse:(id)sender {
    self.self.theManZodiacIndex = 0;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacMouseButton.titleLabel.text, @"年"];
    [self handleZodiac];
}

- (IBAction)touchUpInsideCow:(id)sender {
    self.theManZodiacIndex = 1;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacCowButton.titleLabel.text, @"年"];
    [self handleZodiac];
}

// TODO リファクタリング対象
- (IBAction)tora:(id)sender {
    self.theManZodiacIndex = 2;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacToraButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)usagi:(id)sender {
    self.theManZodiacIndex = 3;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacRabbitButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)tatsu:(id)sender {
    self.theManZodiacIndex = 4;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacDragonButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)hebi:(id)sender {
    self.theManZodiacIndex = 5;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacSnakeButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)uma:(id)sender {
    self.theManZodiacIndex = 6;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacHouseButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)hitsuji:(id)sender {
    self.theManZodiacIndex = 7;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacSheepButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)saru:(id)sender {
    self.theManZodiacIndex = 8;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacMonkeyButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)tori:(id)sender {
    self.theManZodiacIndex = 9;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacBirdButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)dog:(id)sender {
    self.theManZodiacIndex = 10;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacDogButton.titleLabel.text, @"年"];
    [self handleZodiac];
}
- (IBAction)inoshishi:(id)sender {
    self.theManZodiacIndex = 11;
    self.theManZodiacLabel.text = [NSString stringWithFormat:@"%@ %@",self.zodiacWildBoarButton.titleLabel.text, @"年"];
    [self handleZodiac];
}



/** 年代推測スライダー変更 */
- (IBAction)onChangedYearEstimationSlider:(id)sender {
//ラベルの表示を生年月日の年に変更
//    self.selectedEstimationYearLabel.text = [NSString stringWithFormat:@"%d", (int)self.yearEstimationSlider.value];
    [self handleYearEvent];
}

- (IBAction)onChangedAgeEstimanationSlider:(id)sender {
    isInputYear = YES;
    self.nextButton.enabled = true;
    [self prophetAgeForAgeSlider];
}


// TODO 削除予定
// 干支選択イベントハンドリング
- (void)handleZodiac
{
    isInputZodiac = YES;
    [self prophetAge];
}

// 年選択イベントハンドリング
- (void)handleYearEvent
{
    isInputYear = YES;
    self.nextButton.enabled = true;
    [self prophetAge];
}

// 一つ前の年男に該当する年に移動（ボタンタッチ処理）
// 卯年 年男基準年1903年 現在1975年 前へ1963年
// 1974
// 1974-1903=71
// 71%12=11
// 1974-11=1963
- (IBAction)touchUpInsideToBeforeJustYear:(id)sender {
    int oneYearBefore = self.yearEstimationSlider.value - 1; // ちょうど年男の年が選ばれてる場合はひとつ前の年男の年に移動したい
    int justBaseYear = baseYear + self.theManZodiacIndex;
    int gapYear = oneYearBefore - justBaseYear;
    int justYear = oneYearBefore - (gapYear % 12);
    if (justYear >= self.yearEstimationSlider.minimumValue) {
        self.yearEstimationSlider.value = justYear;
        self.selectedEstimationYearLabel.text = [NSString stringWithFormat:@"%d", justYear];
        [self handleYearEvent];
    }
}

// 一つ後の年男に該当する年に移動（ボタンタッチ処理）
// 卯年 年男基準年1903年 現在1963年 次へ1975年
// 1つ後の年男・・・1963+1=1964
// 年男基準年との差・・・1964-1903=61
// 61%12=1
// 12-1=11
// 1964+11
// 1975
- (IBAction)touchUpInsideToAfterJustYear:(id)sender {
    int oneYearAfter = self.yearEstimationSlider.value + 1; // ちょうど年男の年が選ばれてる場合はひとつ後の年男の年にいきたい
    int justBaseYear = baseYear + self.theManZodiacIndex;
    int gapYear = oneYearAfter - justBaseYear;
    int justYear = oneYearAfter + (12 - (gapYear % 12));
    if (justYear <= self.yearEstimationSlider.maximumValue) {
        self.yearEstimationSlider.value = justYear;
        self.selectedEstimationYearLabel.text = [NSString stringWithFormat:@"%d", justYear];
        [self handleYearEvent];
    }
}


/**
 * 年齢推測ロジック(年齢推測スライダー版)
 */
- (void)prophetAgeForAgeSlider
{
    int estimationYear = nowYear - self.ageEstimationSlider.value;
    self.yearEstimationSlider.value = estimationYear;
    
    //<直近で年男・年女にあたる年> ・・・ <今年> ー (<今年の干支のインデックス> ー <その人の干支のインデックス>
    int justMultiplesTwelveYear = nowYear - (nowZodiacIndex - self.theManZodiacIndex);
    
    //生年月日の年＝<スライダーで選択した年>＋((<直近で年男・年女にあたる年> ー <選択した年>)÷12の余り)
    int birthYear = estimationYear + ((justMultiplesTwelveYear - estimationYear) % 12);
    
    // TODO 次の画面へ移動年齢推定後に
    theManBirthYear = birthYear;
    
    //誕生日を迎えた人の年齢 = 「<今年> ー <生年月日>」
    int afterBirthAge = nowYear - birthYear;
    
    int beforeBirthAge = afterBirthAge - 1;
    
    if (beforeBirthAge < 0) {
        return;
    }
    
    self.selectedEstimationYearLabel.text = [NSString stringWithFormat:@"%d", birthYear];
    
    // 干支と年の両方が入力されたら、年齢表示
    if (isInputZodiac == YES && isInputYear == YES) {
        self.beforeBirthAgeLabel.text = [NSString stringWithFormat:@"%d", beforeBirthAge];
        self.afterBirthAgeLabel.text = [NSString stringWithFormat:@"%d", afterBirthAge];
    }
    
}

// 年齢推測ロジック
- (void)prophetAge
{
    int estimationYear = (int)self.yearEstimationSlider.value;

    //<直近で年男・年女にあたる年> ・・・ <今年> ー (<今年の干支のインデックス> ー <その人の干支のインデックス>
    int justMultiplesTwelveYear = nowYear - (nowZodiacIndex - self.theManZodiacIndex);
    
    //生年月日の年＝<スライダーで選択した年>＋((<直近で年男・年女にあたる年> ー <選択した年>)÷12の余り)
    int birthYear = estimationYear + ((justMultiplesTwelveYear - estimationYear) % 12);
    
    // TODO 次の画面へ移動年齢推定後に
    theManBirthYear = birthYear;

    //誕生日を迎えた人の年齢 = 「<今年> ー <生年月日>」
    int afterBirthAge = nowYear - birthYear;
    self.ageEstimationSlider.value = afterBirthAge;
    
    int beforeBirthAge = afterBirthAge - 1;
    
    if (beforeBirthAge < 0) {
        return;
    }

    self.selectedEstimationYearLabel.text = [NSString stringWithFormat:@"%d", birthYear];
    
    // 干支と年の両方が入力されたら、年齢表示
    if (isInputZodiac == YES && isInputYear == YES) {
        self.beforeBirthAgeLabel.text = [NSString stringWithFormat:@"%d", beforeBirthAge];
        self.afterBirthAgeLabel.text = [NSString stringWithFormat:@"%d", afterBirthAge];
    }

}

@end
