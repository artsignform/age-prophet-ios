//
//  ZodiacViewController.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/02/17.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import "ZodiacViewController.h"
#import "ImageViewController.h"
#import "ViewController.h"

@interface ZodiacViewController ()
{
    int theManZodiacIndex;
    NSString *theManZodiacName;
}

@end

@implementation ZodiacViewController

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    //ViewPickerの列(横方向)数を指定
    return 1;
}

- (NSInteger) pickerView: (UIPickerView*)pView numberOfRowsInComponent:(NSInteger) component {
    //ViewPickerの行(縦方向)数を指定
    switch (component) {
        //1列目の行数
        case 0: return 12;
    }
    return 0;
}

- (NSString*)pickerView: (UIPickerView*) pView titleForRow:(NSInteger) row forComponent:(NSInteger)component {

    NSString *zodiacName;
    switch (component) {
        case 0:
            // TODO 干支のインデックスの定数化
            NSLog(@"ZodiacViewController#pickerView rowは%d", row);

            zodiacName = [self getZodiacName: row];
    }
    
    NSLog(@"ZodiacViewController#pickerView theManZodiacNameは%@", zodiacName);

    return zodiacName;
}

/**
 * 干支名取得
 */
- (NSString *)getZodiacName: (int) index {
    NSString *manZodiacName;

                switch(index){
                    case 0:
                        manZodiacName = @"ねずみ";
                        break;
                        
                    case 1:
                        manZodiacName = @"うし";
                        break;
                        
                    case 2:
                        manZodiacName = @"とら";
                        break;
                        
                    case 3:
                        manZodiacName = @"うさぎ";
                        break;
                        
                    case 4:
                        manZodiacName = @"たつ";
                        break;
                        
                    case 5:
                        manZodiacName = @"へび";
                        break;
                        
                    case 6:
                        manZodiacName = @"うま";
                        break;
                        
                    case 7:
                        manZodiacName = @"ひつじ";
                        break;
                        
                    case 8:
                        manZodiacName = @"さる";
                        break;
                        
                    case 9:
                        manZodiacName = @"とり";
                        break;
                        
                    case 10:
                        manZodiacName = @"いぬ";
                        break;
                        
                    case 11:
                        manZodiacName = @"いのしし";
                        break;
                        
                    default:
                        manZodiacName = @"";
                        break;
        }
    return manZodiacName;
}

/** ピッカー選択 */
- (void) pickerView: (UIPickerView*)pView didSelectRow:(NSInteger) row  inComponent:(NSInteger)component {
    
    //PickerViewを操作したら1列目の選択行番号を変数に入れる
    theManZodiacIndex = [_pickerView selectedRowInComponent:0];
    [self handleZodiac];
    
}

// 干支選択イベントハンドリング
- (void)handleZodiac
{
    theManZodiacName = [self getZodiacName: theManZodiacIndex];
    
    NSLog(@"ZodiacViewController#didSelectRow theManZodiacIndexは%d", theManZodiacIndex);
    
    // 干支画像表示
    [self dispZodicacImage: theManZodiacIndex];
}

// 干支ボタン
- (IBAction)touchUpInsideZodiacMouse:(id)sender {
    theManZodiacIndex = 0;
    [self handleZodiac];
}

- (IBAction)touchUpInsideCow:(id)sender {
    theManZodiacIndex = 1;
    [self handleZodiac];
}

// TODO 名前リファクタリング対象
- (IBAction)tora:(id)sender {
    theManZodiacIndex = 2;
    [self handleZodiac];
}
- (IBAction)usagi:(id)sender {
    theManZodiacIndex = 3;
    [self handleZodiac];
}
- (IBAction)tatsu:(id)sender {
    theManZodiacIndex = 4;
    [self handleZodiac];
}
- (IBAction)hebi:(id)sender {
    theManZodiacIndex = 5;
    [self handleZodiac];
}
- (IBAction)uma:(id)sender {
    theManZodiacIndex = 6;
    [self handleZodiac];
}
- (IBAction)hitsuji:(id)sender {
    theManZodiacIndex = 7;
    [self handleZodiac];
}
- (IBAction)saru:(id)sender {
    theManZodiacIndex = 8;
    [self handleZodiac];
}
- (IBAction)tori:(id)sender {
    theManZodiacIndex = 9;
    [self handleZodiac];
}
- (IBAction)dog:(id)sender {
    theManZodiacIndex = 10;
    [self handleZodiac];
}
- (IBAction)inoshishi:(id)sender {
    theManZodiacIndex = 11;
    [self handleZodiac];
}


- (void)dispZodicacImage : (int) index {
    NSString *imageURL;
    // 干支画像
    // TODO 干支のインデックスの定数化
    switch(index){
        case 0:
            imageURL = @"nezumi.png";
            break;
            
        case 1:
            imageURL = @"usi.png";
            break;
            
        case 2:
            imageURL = @"tora.png";
            break;
            
        case 3:
            imageURL = @"usagi.png";
            break;
            
        case 4:
            imageURL = @"ryu.png";
            break;
            
        case 5:
            imageURL = @"hebi.png";
            break;
            
        case 6:
            imageURL = @"uma.png";
            break;
            
        case 7:
            imageURL = @"hitsuji.png";
            break;
            
        case 8:
            imageURL = @"saru.png";
            break;
            
        case 9:
            imageURL = @"tori.png";
            break;
            
        case 10:
            imageURL = @"inu.png";
            break;
            
        case 11:
            imageURL = @"inosisi.png";
            break;
            
        default:
            imageURL = @"zodiac_all.gif";
            break;
    }
    
    self.zodiacImageView.image = [UIImage  imageNamed:imageURL];

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"PushFromZodiacToImage"] ) {
        ImageViewController *nextViewController = [segue destinationViewController];
        NSLog(@"ZodiacViewController 1");
        
        nextViewController.zodiacIndex = -1;
    } else if ( [[segue identifier] isEqualToString:@"PushZodiacToYear"] ) {
        ViewController *nextViewController = [segue destinationViewController];
        NSLog(@"ZodiacViewController#prepareForSegue to ViewController");
        NSLog(@"ZodiacViewController#prepareForSegue theManZodiacIndexは%d", theManZodiacIndex);
        NSLog(@"ZodiacViewController#prepareForSegue theManZodiacNameは%@", theManZodiacName);
        
        nextViewController.theManZodiacIndex = theManZodiacIndex;
        nextViewController.theManZodiacName = theManZodiacName;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.pickerView.delegate = self;
    
    // ピッカー初期選択
    [self.pickerView selectRow:0 inComponent:0 animated:NO];
    theManZodiacIndex = 0;
    theManZodiacName = [self getZodiacName: theManZodiacIndex];
    
    // 干支画像表示
    [self dispZodicacImage: theManZodiacIndex];

    
    // 背景画像
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
