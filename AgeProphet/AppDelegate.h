//
//  AppDelegate.h
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/01/30.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

// 共通変数化(画面間データ共有)
// TODO クラス構造の改良
@property (nonatomic, retain) NSString *birthYear;

@end
