//
//  ViewController.h
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/01/30.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControllerHelper.h"

@interface ViewController : UIViewController


// TODO ローカルでしか使わないメソッドなので、ローカル化
- (void)prophetAge;
- (void)handleZodiac;
- (void)handleYearEvent;
- (void)initXXX;

@property (weak, nonatomic) IBOutlet UILabel *beforeBirthAgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *afterBirthAgeLabel;
@property (weak, nonatomic) IBOutlet UISlider *yearEstimationSlider; // 年代推測スライダー
@property (weak, nonatomic) IBOutlet UISlider *ageEstimationSlider; // 年齢推測スライダー
@property (weak, nonatomic) IBOutlet UILabel *startScaleEstimatingYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *midScaleEstimatingYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastScaleEstimatingYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedEstimationYearLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedZodiacLabel;
@property (weak, nonatomic) IBOutlet UITextView *theManZodiacLabel;

@property (weak, nonatomic) IBOutlet UITextView *theManZodiacText;

// TODO 干支のボタンは消す方向
@property (weak, nonatomic) IBOutlet UIButton *zodiacMouseButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacCowButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacToraButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacRabbitButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacDragonButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacSnakeButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacHouseButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacSheepButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacMonkeyButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacBirdButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacDogButton;
@property (weak, nonatomic) IBOutlet UIButton *zodiacWildBoarButton;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *zodiacImageView;

@property(assign, nonatomic) int theManZodiacIndex; // その人の干支を表すインデックス
@property(assign, nonatomic) NSString *theManZodiacName;

@end
