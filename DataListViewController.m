//
//  DataListViewController.m
//  AgeProphet
//
//  Created by 森 稔貴 on 2013/07/15.
//  Copyright (c) 2013年 Toshitaka MORI. All rights reserved.
//

#import "DataListViewController.h"
#import "ImageViewController.h"
#import "SaveViewController.h"

/**
 TODO
 データをすべて削除する機能。
 リストに干支の画像。
 */
@interface DataListViewController ()
@end

@implementation DataListViewController
{
}

int dataCount; //データ件数
NSMutableArray *dataStrings; // 表示文字列
NSMutableArray *theManIds; // IDs


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSString *)getZodiacName:(int)zodiacIndex
{
    NSString *_zodiacName;
    switch(zodiacIndex){
        case 0:
            _zodiacName = @"ねずみ";
            break;
            
        case 1:
            _zodiacName = @"うし";
            break;
            
        case 2:
            _zodiacName = @"とら";
            break;
            
        case 3:
            _zodiacName = @"うさぎ";
            break;
            
        case 4:
            _zodiacName = @"たつ";
            break;
            
        case 5:
            _zodiacName = @"へび";
            break;
            
        case 6:
            _zodiacName = @"うま";
            break;
            
        case 7:
            _zodiacName = @"ひつじ";
            break;
            
        case 8:
            _zodiacName = @"さる";
            break;
            
        case 9:
            _zodiacName = @"とり";
            break;
            
        case 10:
            _zodiacName = @"いぬ";
            break;
            
        case 11:
            _zodiacName = @"いのしし";
            break;
            
        default:
            _zodiacName = @"";
            break;
    }
    return _zodiacName;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    NSLog(@"willShowViewController");
	[viewController viewWillAppear:animated];
}

/**
 * 戻った時にも表示リストを更新するため、このタイミングで実施。
 */
- (void)viewWillAppear:(BOOL)animated
{
    // データベース
    NSArray*    paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString*   dir   = [paths objectAtIndex:0];
    FMDatabase* db    = [FMDatabase databaseWithPath:[dir stringByAppendingPathComponent:@"AgeProphet.db"]];
    [db open];
    
    // 件数取得
    NSString* selectCountSql = @"SELECT count(*) c FROM member;";
    FMResultSet *resultCount = [db executeQuery:selectCountSql];
    dataCount = 0;
    while ([resultCount next]) {
        dataCount = [resultCount intForColumn:@"c"];
        NSLog(@"data count :%d", dataCount);
    }
    
    //check
    if ([db hadError]) {
        NSLog(@"DB Select count Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        return;
    }
    
    if(dataCount <= 0) {
        return;
    }
    
    // 配列の初期化
    dataStrings = [NSMutableArray array];
    theManIds = [NSMutableArray array];
    
    // 今年を取得
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    NSUInteger flags;
    
    flags = NSYearCalendarUnit;
    comps = [calendar components:flags fromDate:now];
    int nowYear = comps.year;
    
    NSString* selectSql = @"SELECT id, nickname, birth_year, memo, regist_date, zodiac_index FROM member;";
    FMResultSet *results = [db executeQuery:selectSql];
    int _cnt  = 0;
    while ([results next]) {
        _cnt++;
        // 誕生年(年齢)
        int birthYear = [results intForColumn:@"birth_year"];
        int age = nowYear - birthYear;
        
        int zodiacIndex = [results intForColumn:@"zodiac_index"];
        NSString *zodiacName;
        zodiacName = [self getZodiacName:zodiacIndex];
        
        NSString *_nickName = [results  stringForColumn:@"nickname"];
        //        NSString *_memo = [results  stringForColumn:@"memo"];
        //        NSString *_regist_date = [results  stringForColumn:@"regist_date"];
        int _id = [results  intForColumn:@"id"];
        
        // 可変配列に格納するため NSNumber へ変換
        NSNumber *_idNumber = [[NSNumber alloc] initWithInt:_id];
        [theManIds addObject:_idNumber];
        
        //        NSString *str = [NSString stringWithFormat:@"%d. %@ ： %d年(%d) %@ 「%@」 登録%@", _cnt, _nickName, birthYear, age, zodiacName, _memo, _regist_date];
        
        NSString *str = [NSString stringWithFormat:@"%d. %@(%d) ： %@", _cnt, _nickName, age, zodiacName];
        
        [dataStrings addObject:str];
        
        NSLog(@"リスト : %d %@ %d %@ %@ %d", [results intForColumn:@"id"], [results stringForColumn:@"nickname"], birthYear, [results stringForColumn:@"memo"], [results stringForColumn:@"regist_date"], [results intForColumn:@"zodiac_index"]);
    }
    
    //check
    if ([db hadError]) {
        NSLog(@"DB Select Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        return;
    }
    
    [db close];
    
    [self.tableView reloadData];

}

- (void)viewDidLoad
{
    NSLog(@"viewDidLoad start");
    [super viewDidLoad];
    
    // 背景画像
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"paper1_03.jpg"] drawInRect:self.view.bounds];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    // テーブルビュー
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
  
    // TODO EXC_BAD_ACCESSが発生する可能性が高い？？？
    // 保存画面で削除して画面を戻った時にデータ更新されないため、追加したが、無くても動作することがわかった。
    // tableView reloadData が無かっただけ。
//    self.navigationController.delegate = self;
    
    NSLog(@"viewDidLoad end");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//

/** 行数 */
-(NSInteger)tableView:(UITableView *)tableView
numberOfRowsInSection:(NSInteger)section {
    return dataCount;
}


-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cellForRowAtIndexPath start");


    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [dataStrings objectAtIndex: (NSUInteger) indexPath.row];
    
    NSLog(@"cellForRowAtIndexPath end");
    return cell;

}

/** 
 * セグエ遷移前の処理
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"PushToImage"] ) {
        ImageViewController *nextViewController = [segue destinationViewController];
        nextViewController.zodiacIndex = -1;
    } else if ( [[segue identifier] isEqualToString:@"PushToSave"] ) {
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"didSelectRowAtIndexPath start :%d", indexPath.row);

    [tableView deselectRowAtIndexPath:indexPath animated:YES]; // 選択状態の解除。
    if ([dataStrings count] > indexPath.row) {
        NSNumber *n = [theManIds objectAtIndex: (NSUInteger) indexPath.row];
        int selectedId = [n intValue];
        
        //遷移先（Navi2）クラスのインスタンスを生成
        SaveViewController *saveViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"saveViewController"];
        saveViewController.theManId = selectedId;
        NSLog(@"didSelectRowAtIndexPath theManId :%d", selectedId);

        [[self navigationController] pushViewController:saveViewController animated:YES];
    }
    
    NSLog(@"didSelectRowAtIndexPath end");
}


- (void)viewDidUnload {
    NSLog(@"DataListViewController#viewDidUnload start");
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
    self.navigationController.delegate = nil;
    
    [super viewDidUnload];
    NSLog(@"DataListViewController#viewDidUnload end");
}

- (void)dealloc {
    NSLog(@"DataListViewController#dealloc start");
    self.tableView.dataSource = nil;
    self.tableView.delegate = nil;
    self.navigationController.delegate = nil;
    NSLog(@"DataListViewController#dealloc end");
}

@end
